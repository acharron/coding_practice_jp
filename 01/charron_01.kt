fun main(args: Array<String>) {
    var i = 1
    while (i <= 100) {
        var res = ""

        if (i % 3 == 0) res += "Fizz"
        if (i % 5 == 0) res += "Buzz"
        // 後編
        if (i % 7 == 0) res += "Bizz"
        if (i % 11 == 0) res += "Fazz"

        if (res == "") res = i.toString()

        println(res)

        i++
    }
}