# 02b - 配列の数学 - 後編

ソース : [Advent of Code 2017 day 02](https://adventofcode.com/2017/day/2)

[前編](./02_README.md)

## 説明

`input.txt` にある配列がインプットだとして、以下の計算を行ってください。

* 各行で2つの数字だけがお互いに割れる。その結果を計算
* それぞれの割り算の結果を足して、結果を返す

### 例

```text
5 9 2 8
9 4 7 3
3 8 6 5
```

* 1行目 : 8 と 2 の割りは 4
* 2行目 : 9 と 3 の割りは 3
* 3行目 : 2

よって、結果は `4 + 3 + 2 = 9`

## 結果

* 後編 : `312`
