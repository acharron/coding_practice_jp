# コーディング訓練

## 説明

様々なコーディングの問題を集めているレポジトリです。

その中で問題を選んで、コードレビューを通じてコーディング能力向上を目標としています。 (1対1指導をイメージ)

## 内容

(順番でしなくても大丈夫です)

| 番号 | タイトル                                                            | 内容              | 目安時間 |
|:---|:----------------------------------------------------------------|:----------------|:-----|
| 1  | [FizzBuzz](./01/01_README.md) [(後編)](./01/01b_README.md)        | ループ             | 20分間 |
| 2  | [配列の数学](./02/02_README.md) [(後編)](./02/02b_README.md)           | インプット読み込み / ループ | 1時間  |
| 3  | [GOTO 迷路](./03/03_README.md)                                    | インプット読み込み / ループ | 30分間 |
| 4  | [「()」の計算](./04/04_README.md) [(後編)](./04/04b_README.md)         | インプット読み込み       | 15分間 |
| 5  | [プレゼントラッピング](./05/05_README.md) [(後編)](./05/05b_README.md)      | 数学 / インプット読み込み  | 15分間 |
| 6  | [ロケット打ち上げ](./06/06_README.md) [(後編)](./06/06b_README.md)        | 数学 / while ループ  | 45分間 |
| 7  | [プチパソコンを作りましょう](./07/07_README.md) [(後編)](./07/07b_README.md)   | 配列 / ループ        | 2時間  |
| 8  | [プチパソコンを作りましょう 2](./08/08_README.md) [(後編)](./08/08b_README.md) | 配列 / ループ        | 4時間  |


## ヒント・アドバイス

解決に使える基礎の機能など...

### ファイルの読み込み

* PHP
  * [fread() メソッド](http://www.php.net/manual/ja/function.fread.php)
  * [fgets() メソッド](http://www.php.net/manual/ja/function.fgets.php)
  * [file_get_contents() メソッド](http://www.php.net/manual/ja/function.file-get-contents.php)

* Kotlin
  * `File.readLines()`


## 参考 : 問題まとめのサイト

* https://adventofcode.com/
* https://projecteuler.net/archives
* https://overthewire.org/wargames/
* https://cses.fi/problemset/list
* https://projectlovelace.net/problems/
