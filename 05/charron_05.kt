import java.io.File
import kotlin.math.min

fun main(args: Array<String>) {
    val rows = File("05/input.txt").readLines()

    var total = 0
    var ribbon = 0

    for (row in rows) {
        val dimensions = row.split("x").map { it.toInt() }

        val l = dimensions[0]
        val w = dimensions[1]
        val h = dimensions[2]

        val min = min(l*w, min(l*h, w*h))

        total += 2*l*w + 2*w*h + 2*l*h + min

        // 後編
        val perim = 2 * min(l+w, min(l+h, h+w))
        val volume = l * w * h

        ribbon += perim + volume
    }

    println(total)
    println(ribbon)
}