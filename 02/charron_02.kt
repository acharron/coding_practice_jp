import java.io.File

// 例のファイルで、答えではありません。
fun main(args: Array<String>) {
    val rows = File("02/input.txt").readLines()

    var total = 0
    var total2 = 0

    for (row in rows) {
        val numbers = row.split("\t").map { it.toInt() }

        // 前編
        val min = numbers.min() ?: 0
        val max = numbers.max() ?: 0

        total += max - min

        // 後編
        var i = 0
        while (i < numbers.size) {
            val denom = numbers[i]
            for (num in numbers.filter { it != denom }) {
                if (num % denom == 0) {
                    total2 += num / denom
                    break
                }
            }

            i++
        }
    }

    println("Part 1 : $total")
    println("Part 2 : $total2")
}
