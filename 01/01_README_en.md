# 01 - FizzBuzz

## Goal

Make a program that loops from 1 to 100 and output the index.

However, rules apply:

* When the index is a multiple of 3, do not output the index, but "Fizz"
* When the index is a multiple of 5, do not output the index, but "Buzz"
* When the index is a multiple of 3 and 5, output "FizzBuzz"

## Example

Desired output :

```text
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
...
```
